package dataMigration;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;


// TODO: Auto-generated Javadoc
/**
 * The Class OutputFile.
 */
public class OutputFile {

	private String outputDir = "";
	private String[] filterData;
	private String filename = "";
	private String[] filterItems;
	

	
	/**
	 * Constructor
	 * Instantiates a new output file.
	 *
	 * @param outputPath the output path
	 * @param filename the filename
	 * @param filterData the filter data
	 * @param filterItems the filter items
	 */
	public OutputFile(String outputPath, String filename, String[] filterData, String[] filterItems){
		outputDir = outputPath;
		this.filename = filename;
		this.filterData = filterData;
		this.filterItems = filterItems;
	}
	
	/**
	 * Choose output type, 1 = txt, csv = 2.
	 *
	 * @param desiredType the desired type
	 * @return true, if successful
	 */
	public boolean chooseOutputType(int desiredType){
		boolean result = false;
		if(desiredType == 1){
			result = writeTxtFile();
		}else{
			result = writeCsvFile();
		}
		return result;
	}
	
	/**
	 * Write txt file.
	 *
	 * @return true, if successful
	 */
	public boolean writeTxtFile(){
		
		boolean result = false;
		filename = filename + ".txt";
		try{
			FileWriter fw = new FileWriter(new File(outputDir, filename));
			//Build the first row of the file be the column
			for(int i=0; i<filterItems.length; i++){
				fw.write(filterItems[i] + " - ");
			}
			fw.write("\n");
			//Start from the second line, show the data
			for(int i=0; i<filterData.length; i++){
				fw.write(filterData[i] + "\n");
			}
			fw.flush();
			fw.close();
			result = true;
				
		}catch(FileNotFoundException e){
			System.out.println("Error: Desired directory not exists.");
			result = false;
		}catch(IOException e){
			System.out.println("Error: Output IO Exception");
			result = false;
		}
		return result;
	}
	
	/**
	 * Write csv file.
	 *
	 * @return true, if successful
	 */
	public boolean writeCsvFile(){
		
		boolean result = false;
		filename = filename + ".csv";
		try{
			FileWriter fw = new FileWriter(new File(outputDir, filename));
			//Build the first row of the file be the column
			for(int i=0; i<filterItems.length; i++){
				fw.write(filterItems[i] + " - ");
			}
			fw.write("\n");
			//Start from the second line, show the data
			for(int i=0; i<filterData.length; i++){
				filterData[i] = filterData[i].replace("[","");
				filterData[i] = filterData[i].replace("]","");
				fw.write(filterData[i] + ",");
				fw.write("\n");
			}
			fw.flush();
			fw.close();
			result = true;
				
		}catch(FileNotFoundException e){
			System.out.println("Error: Desired directory not exists.");
			result = false;
		}catch(IOException e){
			System.out.println("Error: Output IO Exception");
			result = false;
		}
		return result;
	}
}
