package dataMigration;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


// TODO: Auto-generated Javadoc
/**
 * The Class FileTXTReading.
 */
public class FileTXTReading extends FileReading{

	private String filepath = "";
	private String errorMsg = "";
	
	
	/**
	 * Constructor
	 * Instantiates a new file txt reading.
	 * @param path the path
	 */
	public FileTXTReading(String path){
		super(path);
		setFilepath(path);
		readFiles();
	}

	/**
	 * Read the content of file
	 */
	public void readFiles(){
		int readStatus = 1;
		try{
			String absolutePath = new File("").getAbsolutePath();
			filepath = getFilepath();
			BufferedReader reader = new BufferedReader(new FileReader(absolutePath + filepath));
			List<List<String>> fileData = new ArrayList<List<String>>();

			String line = reader.readLine();
			
			// Check file is empty or not, if empty then return and do nothing
			if(line == null || line.equals("")){
				reader.close();
				System.out.println("The file is empty! Please input another file!");
				readStatus = 0;
			}else{
				setFileHeader(splitLine(line));
			}	

			if(readStatus != 0){
				line = reader.readLine();
				
				//Read the content start from 2nd line
				while(true){
					List<String> itemList = new ArrayList<String>();
					String[] dataSplitedSet = splitLine(line);
					
					for(int i=0;i<dataSplitedSet.length;i++){
						itemList.add(dataSplitedSet[i]);
					}
					fileData.add(itemList);
					line = reader.readLine();
					if(line.equals("--END--")){
						break;
					}
				}
				
				reader.close();
				setFileData(fileData);
				readStatus = 2;
			}
		}catch(IOException e){
			errorMsg = "Error: IO Exception.";
			System.out.println(errorMsg);
			readStatus = 0;
		}
		setReadStatus(readStatus);
	}
	
	/**
	 * Display saved data before filtering
	 */
	public void displaySavedData(){
		super.displaySavedData();
	}
	
	/**
	 * Gets the filepath.
	 *
	 * @return the filepath
	 */
	public String getFilepath() {
		return super.getFilepath();
	}

	/**
	 * Sets the filepath.
	 *
	 * @param filepath the new filepath
	 */
	public void setFilepath(String filepath) {
		super.setFilepath(filepath);
	}
	
	/**
	 * Gets the file data.
	 *
	 * @return the file data
	 */
	public List<List<String>> getFileData() {
		return super.getFileData();
	}

	/**
	 * Sets the file data.
	 *
	 * @param fileData the new file data
	 */
	public void setFileData(List<List<String>> fileData) {
		super.setFileData(fileData);
	}

	/**
	 * Gets the file header.
	 *
	 * @return the file header
	 */
	public String[] getFileHeader() {
		return super.getFileHeader();
	}

	/**
	 * Sets the file header.
	 *
	 * @param filterItems the new file header
	 */
	public void setFileHeader(String[] filterItems) {
		super.setFileHeader(filterItems);
	}
	
	/**
	 * Split line.
	 *
	 * @param line the line
	 * @return the string[]
	 */
	public String[] splitLine(String line){
		return line.split("\\/+");
	}

	/**
	 * Gets the read status.
	 *
	 * @return the read status
	 */
	public int getReadStatus() {
		return super.getReadStatus();
	}

	/**
	 * Sets the read status.
	 *
	 * @param readStatus the new read status
	 */
	public void setReadStatus(int readStatus) {
		super.setReadStatus(readStatus);
	}
}
