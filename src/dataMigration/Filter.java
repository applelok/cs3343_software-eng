package dataMigration;

import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Class Filter.
 */
public class Filter {

	private List<List<String>> data;
	private String[] fileHeaders;
	private String[] result;
	

	
	/**
	 * Instantiates a new filter.
	 *
	 * @param data the data
	 * @param fileHeader the file header
	 */
	public Filter(List<List<String>> data, String[] fileHeader){
		this.data = data;
		this.fileHeaders = fileHeader;
	}
	
	/**
	 * Sets the result.
	 *
	 * @param result the new result
	 */
	public void setResult(String[] result){
		this.result = result;
	}
	
	/**
	 * Gets the result.
	 *
	 * @return the result
	 */
	public String[] getResult(){
		return this.result;
	}
	
	/**
	 * Display filter result.
	 */
	public void displayFilterResult(){
		System.out.println("This is the result: ");
		for(int i=0; i<this.result.length; i++){
			System.out.println(i+1 + ". " + result[i]);
		}
	}
}
