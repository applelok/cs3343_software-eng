package dataMigration;
import java.io.*;
import java.util.*;

// TODO: Auto-generated Javadoc
/**
 * The Class FileCSVReading.
 */
public class FileCSVReading extends FileReading{

	private String filepath = "";
	private String errorMsg = "";

	
	/**
	 * Constructor
	 * Instantiates a new file csv reading.
	 *
	 * @param path the path
	 */
	 public FileCSVReading(String path){
		super(path);
		setFilepath(path);
		readCsvFiles();
	}
	
	/**
	 * Read csv files.
	 */
	public void readCsvFiles(){
		int readStatus = 1;
		final String delimiter = ",";
		
		try {
			String absolutePath = new File("").getAbsolutePath();
			filepath = getFilepath();
			BufferedReader reader = new BufferedReader(new FileReader(absolutePath + filepath));
			List<List<String>> fileData = new ArrayList<List<String>>();
			String line = reader.readLine();
			
			// Check file is empty or not, if empty then return and do nothing
			if(line == null || line.equals("")){
				reader.close();
				System.out.println("The file is empty! Please input another file!");
				readStatus = 0;
			}else{
				setFileHeader(splitLine(line));
			}
			
			if(readStatus != 0){
				line = reader.readLine();
				
				//Start from 2nd line
				while (line != null){
					List<String> itemList = new ArrayList<String>();
					String[] dataSplit = line.split(delimiter);
					
					for (String split : dataSplit){
						itemList.add(split);
					}
					fileData.add(itemList);
					line = reader.readLine();
				}
				
				reader.close();
				setFileData(fileData);
				readStatus = 3;
			}
		}catch(IOException e){
			errorMsg =  "Error: IO Exception.";
			System.out.println(errorMsg);
			readStatus = 0;
		}
		setReadStatus(readStatus);
	}
	
	/**
	 * Display saved data before filtering.
	 */
	public void displaySavedData(){
		super.displaySavedData();
	}
	
	
	/**
	 * Gets the filepath.
	 *
	 * @return the filepath
	 */
	public String getFilepath() {
		return super.getFilepath();
	}

	/**
	 * Sets the filepath.
	 *
	 * @param filepath the new filepath
	 */
	public void setFilepath(String filepath) {
		super.setFilepath(filepath);
	}
	
	/**
	 * Gets the file data.
	 *
	 * @return the file data
	 */
	public List<List<String>> getFileData() {
		return super.getFileData();
	}

	/**
	 * Sets the file data.
	 *
	 * @param fileData the new file data
	 */
	public void setFileData(List<List<String>> fileData) {
		super.setFileData(fileData);
	}

	/**
	 * Gets the file header.
	 *
	 * @return the file header
	 */
	public String[] getFileHeader() {
		return super.getFileHeader();
	}

	/**
	 * Sets the file header.
	 *
	 * @param filterItems the new file header
	 */
	public void setFileHeader(String[] filterItems) {
		super.setFileHeader(filterItems);
	}

	/**
	 * Split line by ",".
	 *
	 * @param line the line
	 * @return the string[]
	 */
	public String[] splitLine(String line){
		return line.split(",");
	}
	
	/**
	 * Gets the read status.
	 *
	 * @return the read status
	 */
	public int getReadStatus() {
		return super.getReadStatus();
	}

	/**
	 * Sets the read status.
	 *
	 * @param readStatus the new read status
	 */
	public void setReadStatus(int readStatus) {
		super.setReadStatus(readStatus);
	}

}
