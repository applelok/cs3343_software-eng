package dataMigration;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

// TODO: Auto-generated Javadoc
/**
 * The Class FilterBySpecificData.
 */
public class FilterBySpecificData extends Filter{
	
	private List<List<String>> data;
	private String[] fileHeaders;
	private String[] result = null;
	private int userChoice;
	private String[] userInputArr = null;
	
	
	/**
	 * Constructor
	 * Instantiates a new filter by specific data.
	 *
	 * @param data the data
	 * @param fileHeader the filter item
	 */
	public FilterBySpecificData(List<List<String>> data, String[] fileHeader){
		super(data, fileHeader);
		this.data = data;
		this.fileHeaders = fileHeader;
	}
	
	/**
	 * Gets the user item choice.
	 *
	 */
	public void getUserItemChoice(){
		
		Scanner scanUser = new Scanner(System.in);
		boolean inputValid;
		do{
			inputValid = true;
			for(int i=0;i<fileHeaders.length;i++){
				System.out.println(i + ": " + fileHeaders[i]);
			}
			System.out.println("Please input how many item you would like to filter:");
			
			this.userChoice = Integer.parseInt(scanUser.nextLine());
			if(this.userChoice>fileHeaders.length){
				inputValid = false;
			}else{
				inputValid = true;
			}
		}while(!inputValid);
		
	}
	
	/**
	 * Gets the user chosen data for filter by data
	 *
	 */
	public void getUserChosenData(){
		Scanner scanUser = new Scanner(System.in);
		boolean validInput;
		do{
			validInput = true;
			System.out.println("Please input the data you want, if the data you want is more than 1, please use ',' to separate them.");
			String userInput = scanUser.nextLine();
			userInputArr = userInput.split(",");
			for(int i=0;i<userInputArr.length;i++){
				int temp = Integer.parseInt(userInputArr[i]);
				if(temp > fileHeaders.length){
					validInput = false;
					System.out.println("Invalid Input.");
				}
			}
		}while(!validInput);
	}
	
	/**
	 * Data filter process by specific data
	 *
	 * @return true, if successful
	 */
	public boolean dataFilterprocess(){
		
		int colIdx = userChoice;
		String choice[] = userInputArr;
		List<String> dataFiltered = new ArrayList<String>();
		 
		for(int i=0;i<data.size();i++){
			String currentData = data.get(i).get(colIdx);
			for(int j=0;j<choice.length;j++){
				if(currentData.equals(choice[j])){
					String matchData = (data.get(i)).toString();
					dataFiltered.add(matchData);
				}
			}
			
		}
		
		result = new String[dataFiltered.size()];
		for(int i=0;i<dataFiltered.size();i++){
			result[i] = dataFiltered.get(i);
		}
		if(result.length>0){
			super.setResult(result);
			return true;
		}else{
			System.out.println("No result has found.");
			return false;
		}
	}
	
	/**
	 * Gets the result.
	 *
	 * @return the result
	 */
	public String[] getResult(){
		return super.getResult();
	}
	
	/**
	 * Display filter result.
	 */
	public void displayFilterResult(){
		super.displayFilterResult();
	}
	
	
}
