package dataMigration;

import java.util.List;
import java.util.Scanner;

// TODO: Auto-generated Javadoc
/**
 * The Class FilterByColumn.
 */
public class FilterByColumn extends Filter{

	private List<List<String>> data;
	private String[] fileHeaders;
	private String[] result;
	private int noOfItem;
	private int[] itemIdx;
	/**
	 * Constructor
	 * Instantiates a new filter by column.
	 *
	 * @param data the file data
	 * @param fileHeader the fileHeader
	 */
	public FilterByColumn(List<List<String>> data, String[] fileHeader){
		super(data,fileHeader);
		this.data = data;
		this.fileHeaders = fileHeader;
	}
	
	/**
	 * Gets the user choice of which column(s) does she/he wants to get.
	 *
	 */
	public void getUserChoice(){
		Scanner scanner = new Scanner(System.in);
		String[] itemIdxStr = new String[noOfItem];
		boolean validInput;
		do{
			validInput = true;
			System.out.println("Please input the index of the item with ','.");
			String userInput = scanner.nextLine();
			itemIdxStr = userInput.split(",");
			
			itemIdx = new int[itemIdxStr.length];
			for(int i=0;i<itemIdxStr.length;i++){
				 int temp = Integer.parseInt(itemIdxStr[i]);
				 if(temp > fileHeaders.length){
					 System.out.println("Invalid input of index of item.");
					 validInput = false;
					 break;
				 }
				 itemIdx[i] = temp;
			}
		}while(!validInput);
	}
	
	/**
	 * Filter the data that the user wants
	 *
	 * @return true, if successful
	 */
	public boolean itemFilterProcess(){
		result = new String[data.size()];
		
		for(int i=0; i<data.size();i++){
			for(int j=0;j<itemIdx.length;j++){
				if(j==0){
					result[i] = data.get(i).get(itemIdx[j]);
				}else{
					result[i] += " - " + data.get(i).get(itemIdx[j]) ;
				}
			}
		}
		if(result.length>0){
			super.setResult(result);
			return true;
		}
		else{
			System.out.println("No result has found.");
			return false;
		}
	}
	
	/**
	 * Gets the filter result.
	 *
	 * @return the result
	 */
	public String[] getResult(){
		return super.getResult();
	}
	
	/**
	 * Display filter result.
	 */
	public void displayFilterResult(){
		super.displayFilterResult();
	}
	
	/**
	 * Get number of items to filter from user
	 */
	public void askNumberOfItems(){
		Scanner scanner = new Scanner(System.in);
		boolean inputType = false;
		int temp = 0;
		do{
			for(int i=0;i<this.fileHeaders.length;i++){
				System.out.println(i + ": " + fileHeaders[i]);
			}
			System.out.println("Please input how many item you would like to filter:");
			temp = Integer.parseInt(scanner.nextLine());
			if(temp > fileHeaders.length){
				System.out.println("Invalid input.");
				inputType = false;
			}else{
				inputType = true;
			}
			
		}while(!inputType);
		noOfItem = temp;
	}
	
	
}
