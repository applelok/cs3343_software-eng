package dataMigration;
import java.util.List;
import java.util.Scanner;


// TODO: Auto-generated Javadoc
/**
 * The Class main.
 */
public class main {

	private static String filePath;
	private static int fileStatus;
	private static FileTXTReading fileTxtRead;
	private static FileCSVReading fileCsvRead;
	private static String[] fileHeader;
	private static List<List<String>> fileData;
	private static String[] filteredData = null;
	private static FilterByColumn filterCol;
	private static FilterBySpecificData filterData;
	private static OutputFile output;
	private static Scanner scan = new Scanner(System.in);

	//Main function
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args){
		//Init variable
		String userInputChoice = "";
		System.out.println("Welcome to Data Migration Program.\n");
		do{
			displayMenu();
			System.out.print("Please input your choice: ");
			userInputChoice = scan.nextLine();
			//If result is 0 = error
			//If result is 2 = txt read success
			//If result is 3 = csv read success
			if(userInputChoice.equals("1")){
				System.out.print("\nPlease input your file path: ");
				String path = scan.nextLine();
				readFile(path);
			}else if(userInputChoice.equals("2")){
				displayData(fileStatus);
			}
			else if(userInputChoice.equals("3")){
				if(fileStatus != 2 && fileStatus !=3){
					System.out.println("Please insert a valid file before filtering");
				}else{
					System.out.println("1. Filter by column name ");
					System.out.println("2. Filter by specific data");
					int choice = scan.nextInt();
					filterData(choice);
				}
			}
			else if(userInputChoice.equals("4")){
				if(fileStatus != 2 && fileStatus !=3){
					System.out.println("Please insert a valid file before output filtered file.");
				}else{
					System.out.println("Please input the output file directory: ");
					String outputPath = scan.nextLine();
					System.out.println("Please input your desired filename: ");
					String filename = scan.nextLine();
					System.out.println("Your desired type of the file?\n 1.txt \n 2.csv");
					String outputType = scan.nextLine();
					outputData(outputPath, filename, filteredData, fileHeader, outputType);
				}
			}
			else if(userInputChoice.equals("5")){
				System.out.println("\nProgram ended. Thanks for using.");
			}
			else{
				//Input value not within 1 - 5
				System.out.println("\nWrong input, please select a choice from the menu below!");
			}
		}while(userInputChoice.equals("5")==false);
		
	}
	
	/**
	 * Display menu.
	 */
	public static void displayMenu(){
		System.out.println("\n------------------Menu------------------");
		System.out.println("1. Read file");
		System.out.println("2. Display the data from read file");
		System.out.println("3. Filter data");
		System.out.println("4. Output data as text file");
		System.out.println("5. Exit program\n");
	}
	
	/**
	 * 1. Read file
	 * file Status 2 = txt file
	 * file Status 3 = csv file
	 * else file Status = error
	 */
	public static void readFile(String path) {
		setFilePath(path);
		checkFileStatus(path);
		
		if(main.fileStatus == 2) {
			main.fileHeader = fileTxtRead.getFileHeader();
			main.fileData = fileTxtRead.getFileData();
		}
		else if(main.fileStatus == 3) {
			main.fileHeader = fileCsvRead.getFileHeader();
			main.fileData = fileCsvRead.getFileData();
		}
		else {
			System.out.print("\n Read file is not valid!");
		}
	}
	
	/**
	 * // 2. Display the data from read file
	 *
	 * @param fileStatus the file status
	 */
	public static void displayData(int fileStatus) {
		if(fileStatus == 2) {
			main.fileTxtRead.displaySavedData();
		}
		else if(fileStatus == 3) {
			main.fileCsvRead.displaySavedData();
		}
		else {
			System.out.print("\n Please input a file before display data!");
		}	
	}
	
	/**
	 * 3. Filter data, divided in 2 types = column and specific data
	 */
	public static void filterData(int choice){
		boolean result;
		if(choice==1){
			FilterByColumn filter = new FilterByColumn(main.fileData, main.fileHeader);
			filter.askNumberOfItems();
			filter.getUserChoice();
			result = filter.itemFilterProcess();
			if(result){
				filter.displayFilterResult();
				main.filteredData = filter.getResult();
			}
			
		}else if(choice==2){
			FilterBySpecificData filter = new FilterBySpecificData(main.fileData, main.fileHeader);
			filter.getUserItemChoice();
			filter.getUserChosenData();
			result = filter.dataFilterprocess();
			if(result){
				filter.displayFilterResult();
				main.filteredData = filter.getResult();
			}
		}
	}
	

	/**
	 * 4. Output data, user can choose output as txt or csv file
	 */
	public static void outputData(String outputPath, String filename, String[] filteredData, String[] fileHeader, String outputType) {
		output = new OutputFile(outputPath, filename, filteredData, fileHeader);
		boolean result = false;
		result = output.chooseOutputType(Integer.parseInt(outputType));
		if(result){
			System.out.println("Output success.");
		}else{
			System.out.println("Output failed.");
		}
	}

	/**
	 * Gets the file path.
	 *
	 * @return the file path
	 */
	public static String getFilePath() {
		return filePath;
	}

	/**
	 * Sets the file path.
	 *
	 * @param filePath the new file path
	 */
	public static void setFilePath(String filePath) {
		main.filePath = filePath;
	}
	
	/**
	 * Gets the file status.
	 *
	 * @return the file status
	 */
	public static int getFileStatus() {
		return fileStatus;
	}

	/**
	 * Sets the file status.
	 *
	 * @param fileStatus the new file status
	 */
	public static void setFileStatus(int fileStatus) {
		main.fileStatus = fileStatus;
	}
	
	/**
	 * Gets the file header.
	 *
	 * @return the file header
	 */
	public static String[] getFileHeader() {
		return fileHeader;
	}

	/**
	 * Sets the file header.
	 *
	 * @param fileHeader the new file header
	 */
	public void setFileHeader(String[] fileHeader) {
		this.fileHeader = fileHeader;
	}
	
	/**
	 * Gets the file data.
	 *
	 * @return the file data
	 */
	public static List<List<String>> getFileData() {
		return fileData;
	}

	/**
	 * Sets the file data.
	 *
	 * @param fileData the new file data
	 */
	public void setFileData(List<List<String>> fileData) {
		this.fileData = fileData;
	}
	
	public static String[] getFilteredData() {
		return filteredData;
	}

	public static void setFilteredData(String[] filteredData) {
		main.filteredData = filteredData;
	}

	

	/**
	 * check file status, 0 = error , 2 = txt, 3 = csv
	 *
	 * @param filePath the file path
	 */
	public static void checkFileStatus(String filePath){
		
		if (filePath.contains(".txt")){
			FileTXTReading fileTxtRead = new FileTXTReading(filePath);
			main.fileTxtRead = fileTxtRead;
			setFileStatus(main.fileTxtRead.getReadStatus());
		}
		else if (filePath.contains(".csv")){
			FileCSVReading fileCsvRead = new FileCSVReading(filePath);
			main.fileCsvRead = fileCsvRead;
			setFileStatus(main.fileCsvRead.getReadStatus());
		}
		else {
			setFileStatus(0);
			//fileReadResult = "Not .txt file or .csv file";
		}
	}
		
}
