/*
 * 
 */
package dataMigration;

import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Class FileReading.
 */
public class FileReading {

	private String filepath = "";
	private String[] fileHeader;
	private List<List<String>> fileData;
	private int readStatus;
	
	/**
	 * Instantiates a new file reading.
	 *
	 * @param path the path
	 */
	public FileReading(String path){
		this.filepath = path;
	}
	
	
	/**
	 * Display saved data.
	 */
	public void displaySavedData(){
		
		System.out.println("\nFollowings are the data inside the dataset: \n");

		for(int i=0;i<this.fileData.size();i++){
			for(int j=0;j<fileHeader.length;j++){
				System.out.print(this.fileData.get(i).get(j) + " - ");
			}
			System.out.println();
		}
		System.out.println("\nTotal rows inside dataset is " + this.fileData.size());
	}
	
	/**
	 * Gets the filepath.
	 *
	 * @return the filepath
	 */
	public String getFilepath() {
		return this.filepath;
	}

	/**
	 * Sets the filepath.
	 *
	 * @param filepath the new filepath
	 */
	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}
	
	/**
	 * Gets the file data.
	 *
	 * @return the file data
	 */
	public List<List<String>> getFileData() {
		return fileData;
	}

	/**
	 * Sets the file data.
	 *
	 * @param fileData the new file data
	 */
	public void setFileData(List<List<String>> fileData) {
		this.fileData = fileData;
	}

	/**
	 * Gets the file header.
	 *
	 * @return the file header
	 */
	public String[] getFileHeader() {
		return this.fileHeader;
	}

	/**
	 * Sets the file header.
	 *
	 * @param filterItems the new file header
	 */
	public void setFileHeader(String[] filterItems) {
		this.fileHeader = filterItems;
	}
	

	/**
	 * Gets the read status.
	 *
	 * @return the read status
	 */
	public int getReadStatus() {
		return this.readStatus;
	}

	/**
	 * Sets the read status.
	 *
	 * @param readStatus the new read status
	 */
	public void setReadStatus(int readStatus) {
		this.readStatus = readStatus;
	}
}
