package testcase;
import java.io.ByteArrayInputStream;
import java.util.List;

import junit.framework.TestCase;
import dataMigration.FileCSVReading;
import dataMigration.FileTXTReading;
import dataMigration.FilterByColumn;
import dataMigration.FilterBySpecificData;
import dataMigration.OutputFile;
import dataMigration.main;

public class Test extends TestCase {


	private String txtTestpath = "//src//testcase//simpleTest.txt";
	private String csvTestpath = "//src//testcase//CSVTestfile.csv";
	private String emptyTxtpath = "//src//testcase//EmptyFile.txt";
	private String emptyCsvpath = "//src//testcase//EmptyCSV.csv";
	private static String filePath;
	private static int fileStatus;
	private static FileTXTReading fileTxtRead;
	private static FileCSVReading fileCsvRead;
	private static String[] fileHeader;
	private static List<List<String>> fileData;
	private static String[] filteredData;
	
	
	// initial datasource, used to override main.checkFileStatus(String filePath)
	// all the read data will store at testcase.Test.fileTxtRead and testcase.Test.fileCsvRead
	// with testcase.Test.fileStatus
	// testcase.Test.fileStatus can be changed by calling testcase.Test.initialFileStatus(int fileStatus)
	public static void initialDataSource(String filePath){
		initialFilePath(filePath);
		if (filePath.contains(".txt")){
			FileTXTReading fileTxtRead = new FileTXTReading(filePath);
			testcase.Test.fileTxtRead = fileTxtRead;
			testcase.Test.fileStatus = testcase.Test.fileTxtRead.getReadStatus();
		}
		else if (filePath.contains(".csv")){
			FileCSVReading fileCsvRead = new FileCSVReading(filePath);
			testcase.Test.fileCsvRead = fileCsvRead;
			testcase.Test.fileStatus = testcase.Test.fileCsvRead.getReadStatus();
		}
		else {
			testcase.Test.fileStatus = 0;
		}
		
		if(testcase.Test.fileStatus == 2) {
			testcase.Test.fileHeader = testcase.Test.fileTxtRead.getFileHeader();
			testcase.Test.fileData = testcase.Test.fileTxtRead.getFileData();
		}
		else if(testcase.Test.fileStatus == 3) {
			testcase.Test.fileHeader = testcase.Test.fileCsvRead.getFileHeader();
			testcase.Test.fileData = testcase.Test.fileCsvRead.getFileData();
		}
		else {
			System.out.print("\n Read file is not valid!");
		}
	}
	
	public static void initialFilePath(String filePath) {
		testcase.Test.filePath = filePath;
	}
	
	public static void initialFileStatus(int fileStatus) {
		testcase.Test.fileStatus = fileStatus;
	}

	// 1. Read file
	// test data source type, txt
	// txt should be 2
	public void test01(){
		String dataSource = txtTestpath;
		int expectedResult = 2;
		
		main.readFile(dataSource);
		int result = main.getFileStatus();
		assertEquals(result, expectedResult);
	}
	
	// 2. Read file
	// test data source type, csv
	// txt should be 3
	public void test02(){
		String dataSource = csvTestpath;
		int expectedResult = 3;
		
		main.readFile(dataSource);
		int result = main.getFileStatus();
		assertEquals(result, expectedResult);
	}
	
	// 3. Filter data
	// "1. Filter by column name ", choice = 2
	// item you would like to filter, userChoice = 1,2
	public void test03(){
		String dataSource = txtTestpath;
		String noOfItems = "2";
		String userChoice = "1,2";
		initialDataSource(dataSource);
		FilterByColumn filter = new FilterByColumn(fileData, fileHeader);
		System.setIn(new ByteArrayInputStream(noOfItems.getBytes()));
	  	filter.askNumberOfItems();
		System.setIn(new ByteArrayInputStream(userChoice.getBytes()));
	  	filter.getUserChoice();
	  	boolean result = filter.itemFilterProcess();
		boolean expectedResult = true;
		
		if(result){
			filter.displayFilterResult();
		}
		assertEquals(result,expectedResult);
	}
	
	// 4. Filter data
	// "1. Filter by specific name ", choice = 2
	// data you would like to filter, userChoice = 1
	public void test04(){
		String dataSource = txtTestpath;
		String preferColumn = "2";
		String userChoice = "1";
		initialDataSource(dataSource);
		FilterBySpecificData filter = new FilterBySpecificData(fileData, fileHeader);
		System.setIn(new ByteArrayInputStream(preferColumn.getBytes()));
		filter.getUserItemChoice();
		System.setIn(new ByteArrayInputStream(userChoice.getBytes()));
		filter.getUserChosenData();
		boolean result = filter.dataFilterprocess();
		if(result){
			filter.displayFilterResult();
		}
		boolean expectedResult = true;
		assertEquals(result, expectedResult);
	}
	
	// 5. Output data as txt file
	// "1. Filter by specific name ", choice = 2
	// data you would like to filter, userChoice = 1
	public void test05(){
		String dataSource = txtTestpath;
		String preferColumn = "2";
		String userChoice = "1";
		initialDataSource(dataSource);
		FilterBySpecificData filter = new FilterBySpecificData(fileData, fileHeader);
		System.setIn(new ByteArrayInputStream(preferColumn.getBytes()));
		filter.getUserItemChoice();
		System.setIn(new ByteArrayInputStream(userChoice.getBytes()));
		filter.getUserChosenData();
		boolean filterResult = filter.dataFilterprocess();
		String[] filterData = null;
		if(filterResult){
			filter.displayFilterResult();
			filterData = filter.getResult();
		}
		main.outputData("C://Users//Public//Documents", "abc", filterData, fileHeader, "1");
		
	}
	// 6. Output data as csv file
	// "1. Filter by specific name ", choice = 2
	// data you would like to filter, userChoice = 1
	public void test06(){
		String dataSource = csvTestpath;
		String preferColumn = "2";
		String userChoice = "1";
		initialDataSource(dataSource);
		FilterBySpecificData filter = new FilterBySpecificData(fileData, fileHeader);
		System.setIn(new ByteArrayInputStream(preferColumn.getBytes()));
		filter.getUserItemChoice();
		System.setIn(new ByteArrayInputStream(userChoice.getBytes()));
		filter.getUserChosenData();
		boolean filterResult = filter.dataFilterprocess();
		String[] filterData = null;
		if(filterResult){
			filter.displayFilterResult();
			filterData = filter.getResult();
		}
		main.outputData("C://Users//Public//Documents", "abc", filterData, fileHeader, "2");
		
	}
	
	//Test case 7: check the fileSaved data is really the same as the actual length
	//For TXT
	public void test07(){
		FileTXTReading reader = new FileTXTReading(txtTestpath);
		reader.displaySavedData();
		int result = reader.getFileData().size();
		int expectedResult = 2;
		assertEquals(result, expectedResult);
	}
	
	//Test case 8: check the fileSaved data is really the same as the actual length
	//For CSV
	public void test08(){
		FileCSVReading reader = new FileCSVReading(csvTestpath);
		reader.displaySavedData();
		int result = reader.getFileData().size();
		int expectedResult = 3;
		assertEquals(result, expectedResult);
	}
	//Test case 9: check fileException for txt
	public void test09(){
		FileTXTReading reader = new FileTXTReading("D://ABC");
		int result = reader.getReadStatus();
		int expectedResult = 0;
		assertEquals(result, expectedResult);
	}
	
	//Test case 10: check fileException for csv
	public void test10(){
		FileCSVReading reader = new FileCSVReading("D://ABC");
		int result = reader.getReadStatus();
		int expectedResult = 0;
		assertEquals(result, expectedResult);
	}
	
	//Test case 11: check fileException for csv
	public void test11(){
		String dataSource = csvTestpath;
		String preferColumn = "2";
		String userChoice = "1";
		initialDataSource(dataSource);
		FilterBySpecificData filter = new FilterBySpecificData(fileData, fileHeader);
		System.setIn(new ByteArrayInputStream(preferColumn.getBytes()));
		filter.getUserItemChoice();
		System.setIn(new ByteArrayInputStream(userChoice.getBytes()));
		filter.getUserChosenData();
		boolean filterResult = filter.dataFilterprocess();
		String[] filterData = null;
		if(filterResult){
			filter.displayFilterResult();
			filterData = filter.getResult();
		}
		OutputFile output = new OutputFile("C://ABC", "abc", filterData, fileHeader);
		boolean result = output.writeTxtFile();
		boolean expectedResult = false;
		assertEquals(result, expectedResult);
	}
	
	//Test case 12: check fileException for txt
	public void test12(){
		String dataSource = csvTestpath;
		String preferColumn = "2";
		String userChoice = "1";
		initialDataSource(dataSource);
		FilterBySpecificData filter = new FilterBySpecificData(fileData, fileHeader);
		System.setIn(new ByteArrayInputStream(preferColumn.getBytes()));
		filter.getUserItemChoice();
		System.setIn(new ByteArrayInputStream(userChoice.getBytes()));
		filter.getUserChosenData();
		boolean filterResult = filter.dataFilterprocess();
		String[] filterData = null;
		if(filterResult){
			filter.displayFilterResult();
			filterData = filter.getResult();
		}
		OutputFile output = new OutputFile("C://ABC", "abc", filterData, fileHeader);
		boolean result = output.writeCsvFile();
		boolean expectedResult = false;
		assertEquals(result, expectedResult);
		
	}
	
	//Test case 13: check emptyFile for txt
	public void test13(){
		FileTXTReading reader = new FileTXTReading(emptyTxtpath);
		int result = reader.getReadStatus();
		int expectedResult = 0;
		assertEquals(result, expectedResult);
	}
	
	//Test case 14: check emptyFile for csv
	public void test14(){
		FileTXTReading reader = new FileTXTReading(emptyCsvpath);
		int result = reader.getReadStatus();
		int expectedResult = 0;
		assertEquals(result, expectedResult);
	}
//	
//	public void test05(){
//		String dataType = txtTestpath;
//		boolean expectedResult = true;
//		int[] userChoice = {2,3};
//		
//		initialDataSource(dataType);
//		FilterBySpecificData filter = new FilterBySpecificData(data,fileFilterItem);
//		boolean result = filter.dataFilterprocess();
//		assertEquals(result,true);
//	}
//	
//	
//	public void test06(){
//		CheckFileType fileType = new CheckFileType();
//		int checkFileResult = fileType.checkFileStatus(txtTestpath);
//		String[] fileFilterItem = fileType.getFilterItemsbyType(checkFileResult);
//		List<List<String>> data = fileType.getDatabyType(checkFileResult);
//		FilterBySpecificData filter = new FilterBySpecificData(data,fileFilterItem);
//		boolean result = filter.dataFilterprocess();
//		String[] filteredData = filter.getResult();
//		OutputFile output = new OutputFile("C://Users//Apple//Downloads", "test06", filteredData,fileFilterItem);
//		boolean outputResult = output.chooseOutputType(Integer.parseInt("1"));
//		assertEquals(outputResult, true);
//	}
//	
//	public void test07(){
//		CheckFileType fileType = new CheckFileType();
//		int checkFileResult = fileType.checkFileStatus(txtTestpath);
//		String[] fileFilterItem = fileType.getFilterItemsbyType(checkFileResult);
//		List<List<String>> data = fileType.getDatabyType(checkFileResult);
//		FilterBySpecificData filter = new FilterBySpecificData(data,fileFilterItem);
//		boolean result = filter.dataFilterprocess();
//		String[] filteredData = filter.getResult();
//		OutputFile output = new OutputFile("C://Users//Apple//Downloads", "test06", filteredData,fileFilterItem);
//		boolean outputResult = output.chooseOutputType(Integer.parseInt("2"));
//		assertEquals(outputResult, true);
//	}
////	public void testFilterMale(){
////		boolean result;
////		result = filter.isMaleOnly();
////		assertEquals(result,true);
////	}
////	
////	public void testFilterFemale(){
////		boolean result;
////		result = filter.isFemaleOnly();
////		assertEquals(result,true);
////	}
//	
////	public void testFilterItemsChoices(){
////		String testFile = "/src/testcase/sourceData2.txt";
////		fileRead = new FileTXTReading(testFile);
////		fileRead.readFiles();
//////		fileRead.filterItemsChoices(fileRead.getSavedDataList());
////		String[] result = fileRead.getFilterItems();
////		String[] expected = {"LastName","FirstName","Gender","Age"};
////		boolean match = false;
////		for(int i=0;i<expected.length;i++){
////			if(!result[i].equals(expected[i])){
////				break;
////			}
////			match=true;
////		}
////		assertEquals(match,true);		
////	}
//	
//	public void testFiltering(){
////		String testFile = "/src/testcase/sourceData2.txt";
////		fileRead = new FileTXTReading(testFile);
////		fileRead.readFiles();
////		itemsFilter = new ItemsFiltering(fileRead.getSavedDataList());
////		itemsFilter.itemFilterProcess(Integer.parseInt("1"));
////		String result = itemsFilter.showFilteredData();
////		String expected = "Done filtering.";
////		assertEquals(result,expected);		
//	}
//	
//	//
//	public void testFilteringGender(){
////		String testFile = "/src/testcase/simpleTest.txt";
////		fileRead = new FileTXTReading(testFile);
////		fileRead.readFiles();
////		itemsFilter = new ItemsFiltering(fileRead.getSavedDataList());
////		String[] result = itemsFilter.dataFilterprocess("2","1");		
////		String[] expected = {"Sutton, Hoyt, 1, 25"};
////		assertEquals(result.length,expected.length);
//	}
//	
////	public void testReadFile(){
////		String testFile = "/src/testcase/sourceData2.txt";
////		String expected = "Input Success!";
////		fileRead = new FileTXTReading(testFile);
////		String result = fileRead.readFiles();
////		assertEquals(result,expected);
////	}
////	
////	public void testCannotReadPath(){
////		String result;
////		String testFilename = "/src/testcase/sourceData123.txt";
////		String expected = "Error: File not found.";
////		fileRead = new FileTXTReading(testFilename);
////		result = fileRead.readFiles();
////		assertEquals(result,expected);
////	}
//	
//	public void testInputItemsChoices(){
//		//TODO: check inputchoice is exists in dataset
//	}
//	
//	public void testOutputDirectoryExists(){
////		String result;
////		String testDir = "C:\\UsersABC";
////		String testFileName = "abc.txt";
////		String expected = "Error: Desired directory not exists.";
////		String testDataset[] = {"ABC","DEF"};
////		outputFile = new OutputFile(testDir,testFileName,testDataset);
////		result = outputFile.WriteOutputFile();
////		assertEquals(result,expected);
//	}
//	
////	public void testOutput(){
////		String result;
////		String testDir = "C:\\Users\\";
////		String testFileName = "abc";
////		String expected = "Output Success.";
////		String testDataset[] = {"ABC","DEF"};
////		outputFile = new OutputFile(testDir,testFileName,testDataset);
////		result = outputFile.WriteOutputFile();
////		assertEquals(result,expected);
////	}
////	
////	public void testFunction2(){
////		int choice = 2;
////		FileReading path = null;
////		choice = 2;
////		path = filename;
////		
////	}
}
